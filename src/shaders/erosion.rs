use crate::gpu_data::GpuData;
use crate::gpu_handler::GpuHandler;
use wgpu::util::DeviceExt;
use wgpu::PipelineCompilationOptions;

const WORKGROUP_SIZE: (u32, u32, u32) = (8, 8, 8);

pub struct Erosion {
    pub data_buffer: wgpu::Buffer,
    pub data_bind_group: wgpu::BindGroup,
    pub compute_pipeline: wgpu::ComputePipeline,
    pub work_group_count: [u32; 3],
    pub frame_num: usize,
}
impl Erosion {
    pub fn new<T: GpuData>(gpu: &GpuHandler, data: &T) -> Self {
        let compute_shader = gpu
            .device
            .create_shader_module(wgpu::ShaderModuleDescriptor {
                label: Some("Compute shader"),
                source: wgpu::ShaderSource::Wgsl(include_str!("erosion.wgsl").into()),
            });

        // Buffers
        let data_buffer = gpu
            .device
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some(data.label()),
                contents: data.cast_slice(),
                usage: data.usage(),
            });

        let compute_bind_group_layout =
            gpu.device
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    entries: &[wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStages::COMPUTE,
                        ty: wgpu::BindingType::Buffer {
                            ty: data.binding_type(),
                            has_dynamic_offset: false,
                            min_binding_size: wgpu::BufferSize::new(data.raw_size()),
                        },
                        count: None,
                    }],
                    label: Some("Comput bind group layout"),
                });

        let compute_pipeline_layout =
            gpu.device
                .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    label: Some("Compute layout"),
                    bind_group_layouts: &[&compute_bind_group_layout],
                    push_constant_ranges: &[],
                });

        let compute_pipeline =
            gpu.device
                .create_compute_pipeline(&wgpu::ComputePipelineDescriptor {
                    label: Some("Compute pipeline"),
                    layout: Some(&compute_pipeline_layout),
                    module: &compute_shader,
                    entry_point: "cs_main",
                    compilation_options: PipelineCompilationOptions::default(),
                });

        let data_bind_group = gpu.device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &compute_bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: data_buffer.as_entire_binding(),
            }],
            label: None,
        });

        let dimension = [0, 0, 0];

        let work_group_count = [
            ((dimension[0] as f32) / (WORKGROUP_SIZE.0 as f32)).ceil() as u32,
            ((dimension[1] as f32) / (WORKGROUP_SIZE.1 as f32)).ceil() as u32,
            ((dimension[2] as f32) / (WORKGROUP_SIZE.2 as f32)).ceil() as u32,
        ];

        Erosion {
            data_buffer,
            data_bind_group,
            compute_pipeline,
            work_group_count,
            frame_num: 0,
        }
    }

    pub fn next_step(&mut self, encoder: &mut wgpu::CommandEncoder) {
        // Advance one step
        let mut cpass = encoder.begin_compute_pass(&wgpu::ComputePassDescriptor {
            label: Some("Simulation step"),
            ..Default::default()
        });

        cpass.set_pipeline(&self.compute_pipeline);
        cpass.set_bind_group(0, &self.data_bind_group, &[]);
        //cpass.set_bind_group(1, output_bind_group, &[]);
        cpass.dispatch_workgroups(
            self.work_group_count[0],
            self.work_group_count[1],
            self.work_group_count[2],
        );

        self.frame_num += 1;
    }
}
