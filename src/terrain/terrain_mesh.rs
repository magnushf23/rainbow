use bytemuck::{Pod, Zeroable};

#[derive(Default, Debug, Clone, Copy)]
pub struct TerrainMesh {
    resolution: [usize; 2],
    size: [f64; 2],
    origin: [f64; 2],
    _composition: [f64; 4],
}

#[repr(C)]
#[derive(Clone, Copy, Pod, Zeroable)]
pub struct RawNode {
    _position: [f32; 4],
    _composition: [f32; 4],
}

impl TerrainMesh {
    pub fn get_mesh(self) -> Vec<Vec<[f64; 3]>> {
        (0..self.resolution[0])
            .map(|i| {
                (0..self.resolution[1])
                    .map(|j| {
                        [
                            self.origin[0]
                                + (i as f64) * self.size[0] / (self.resolution[0] as f64),
                            self.origin[1]
                                + (j as f64) * self.size[1] / (self.resolution[1] as f64),
                            0.,
                        ]
                    })
                    .collect()
            })
            .collect::<Vec<Vec<[f64; 3]>>>()
    }

    pub fn get_raw_mesh(self) {}
}
