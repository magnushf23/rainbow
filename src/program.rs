use crate::{
    gpu_handler::GpuHandler,
    imagetexture::ImageTexture,
    renderer::{RenderParameters, Renderer},
};
use std::{sync::Arc, time::Instant};
use winit::{
    application::ApplicationHandler,
    dpi::PhysicalSize,
    event::*,
    event_loop::{ActiveEventLoop, EventLoop},
    keyboard::{Key, NamedKey},
    window::{Window, WindowAttributes, WindowId},
};

pub struct GpuProgram {
    pub gpu: GpuHandler,
    pub renderer: Renderer,
    pub texture: ImageTexture,
    pub dimension: [u32; 2],
}
impl GpuProgram {
    pub async fn new(window: Arc<Window>) -> Self {
        let init_time = Instant::now();

        // Create simulation problem
        let width = 1200;
        let height = 600;
        let dimension = [width, height];

        // Initialize the gpu controller
        let gpu = GpuHandler::new(window).await;

        let render_params = RenderParameters { size: dimension };

        let texture = ImageTexture::new(&gpu, dimension, wgpu::TextureFormat::Rgba8Unorm);

        let renderer = Renderer::new(&gpu, &render_params, &texture);
        //let solver = Solver::new(&gpu);
        println!(
            "\nInitialisation done in {} seconds.\n",
            init_time.elapsed().as_secs_f64()
        );

        //gpu.surface.configure(&gpu.device, &gpu.config);

        GpuProgram {
            gpu,
            //solver,
            renderer,
            texture,
            dimension,
        }
    }

    pub fn input(&mut self, _event: &WindowEvent) -> bool {
        false
    }

    pub fn update(&mut self) {
        // Nothing
    }

    pub fn render(&mut self) -> Result<(), wgpu::SurfaceError> {
        // Create render output surface for sim visualization

        let output = self.gpu.surface.get_current_texture()?;
        let view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        let mut encoder = self
            .gpu
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Compute & Render Encoder"),
            });

        // Update simulation
        for _iteration in 0..1 {
            //self.solver
            //    .next_step(&mut encoder, &self.texture.compute_bind_group);
        }

        // Render
        self.renderer
            .render(&mut encoder, &view, &self.texture.render_bind_group);

        self.gpu.queue.submit(std::iter::once(encoder.finish()));
        //self.gpu.queue.submit(Some(encoder.finish()));
        output.present();
        //println!("Renders...");
        Ok(())
    }
}

struct State {
    window: Option<Arc<Window>>,
    program: Option<GpuProgram>,
    counter: i32,
    render_counter: i32,
    render_stopwatch: Instant,
}
impl Default for State {
    fn default() -> Self {
        State {
            window: None,
            program: None,
            counter: 0,
            render_counter: 0,
            render_stopwatch: Instant::now(),
        }
    }
}

impl ApplicationHandler for State {
    // This is a common indicator that you can create a window.
    fn resumed(&mut self, event_loop: &ActiveEventLoop) {
        let window = Arc::new(
            event_loop
                .create_window(
                    WindowAttributes::default()
                        .with_title("Program")
                        .with_inner_size(PhysicalSize::new(1200, 600)),
                )
                .unwrap(),
        );
        let program = pollster::block_on(GpuProgram::new(Arc::clone(&window)));
        self.window = Some(window);
        self.program = Some(program);
    }
    fn window_event(
        &mut self,
        event_loop: &ActiveEventLoop,
        window_id: WindowId,
        event: WindowEvent,
    ) {
        // `unwrap` is fine, the window will always be available when
        // receiving a window event.
        let window = self.window.as_ref().unwrap();
        let program = self.program.as_mut().unwrap();
        // Handle window event.
        if window_id == window.id() && !program.input(&event) {
            match event {
                WindowEvent::CloseRequested
                | WindowEvent::KeyboardInput {
                    event:
                        KeyEvent {
                            state: ElementState::Pressed,
                            logical_key: Key::Named(NamedKey::Escape),
                            ..
                        },
                    ..
                } => {
                    println!("Stopping program...");
                    event_loop.exit()
                }
                WindowEvent::Resized(physical_size) => {
                    program.gpu.resize(physical_size);
                }
                WindowEvent::ScaleFactorChanged {
                    inner_size_writer: _,
                    ..
                } => {
                    // TODO
                    program.gpu.resize(window.inner_size());
                }
                WindowEvent::RedrawRequested => {
                    match program.render() {
                        Ok(_) => {
                            self.render_counter += 1;
                        }
                        // Reconfigure the surface if lost
                        Err(wgpu::SurfaceError::Lost | wgpu::SurfaceError::Outdated) => {
                            program.gpu.resize(program.gpu.size)
                        }
                        // The system is out of memory, we should probably quit
                        Err(wgpu::SurfaceError::OutOfMemory) => event_loop.exit(),
                        // All other errors (Outdated, Timeout) should be resolved by the next frame
                        Err(e) => eprintln!("{:?}", e),
                    }
                }
                _ => {}
            }
        }
    }
    fn device_event(
        &mut self,
        _event_loop: &ActiveEventLoop,
        _device_id: DeviceId,
        _event: DeviceEvent,
    ) {
        // Handle window event.
    }
    fn about_to_wait(&mut self, _event_loop: &ActiveEventLoop) {
        if let Some(window) = self.window.as_ref() {
            let program = self.program.as_mut().unwrap();
            let elapsed_time = self.render_stopwatch.elapsed().as_secs_f64();
            if elapsed_time >= 1. {
                println!(
                    "FPS: {:.2} | Mean invocation runtime: {:.2e} s",
                    (self.render_counter as f64 / elapsed_time),
                    elapsed_time
                        / (program.dimension[0] * program.dimension[1]) as f64
                        / self.render_counter as f64,
                );
                self.render_counter = 0;
                self.render_stopwatch = Instant::now();
            }
            window.request_redraw();
            self.counter += 1;
        }
    }
}

// Main run loop
pub async fn run() {
    {
        env_logger::init();
    };
    // Initialize event loop
    let event_loop = EventLoop::new().unwrap();

    // Build program
    let mut state = State::default();

    // Run
    let _ = event_loop.run_app(&mut state);
}
