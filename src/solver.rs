const _WORKGROUP_SIZE: (u32, u32, u32) = (8, 8, 8);

pub trait ComputeShader {
    fn execute_once(&mut self, encoder: &mut wgpu::CommandEncoder, output: &wgpu::BindGroup);
}
