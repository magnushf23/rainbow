use rainbow::program::run;

fn main() {
    println!("Starting program!");
    pollster::block_on(run());
}
