//! # Rainbow
//!
//! [Example]

/// GpuHandler struct configures access to the hardware:
/// GPU device, queue, surface and configuration.
pub mod gpu_handler;
/// This defines the `Program` struct and the main run loop.
pub mod program;
/// GPU solver. Creates the compute pipeline and handles the
/// computation on the GPU.
pub mod solver;

pub mod renderer;

pub mod imagetexture;

pub mod terrain;

pub mod shaders;

pub mod gpu_data;
