use bytemuck::AnyBitPattern;

pub trait GpuData {
    fn label(&self) -> &str;

    fn cast_slice<T: AnyBitPattern>(&self) -> &[T];

    fn usage(&self) -> wgpu::BufferUsages;

    fn binding_type(&self) -> wgpu::BufferBindingType;

    fn raw_size(&self) -> u64;
}
